FROM openjdk:8
VOLUME /tmp
EXPOSE 7002
ADD ./build/libs/cron-email-service-0.0.1-SNAPSHOT.jar email-service.jar
ENTRYPOINT ["java","-jar","email-service.jar"]


