package com.cron.email.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cron.email.dto.UserDto;
import com.cron.email.service.UserNotificationServiceImpl;

@RestController
public class UserController {

	private static final Logger looger = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private UserNotificationServiceImpl userNotificationServiceImpl;
	
	@Scheduled(cron = "0 */2 * ? * *")
	@RequestMapping(value = "/sendMail",method = RequestMethod.GET)
	public ResponseEntity<String> executeMail(UserDto user) {

		try {
			userNotificationServiceImpl.userNotification(user);
		} catch (MailException e) {
			e.printStackTrace();
			return  ResponseEntity.ok("ERROR: " + e.getMessage());
		}
		
		return ResponseEntity.ok("el E-mail se envio con exito");
	}
}
