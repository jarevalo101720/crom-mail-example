package com.cron.email.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.cron.email.dto.UserDto;

@Service
public class UserNotificationServiceImpl implements UserNotificationService {

	private static final  Logger logger = LoggerFactory.getLogger(UserNotificationServiceImpl.class);
	
	@Autowired
	private JavaMailSender javamail;
	
	@Override
	
	public void userNotification(UserDto user) {
		SimpleMailMessage mail = new SimpleMailMessage();
				
		try {
			user.setName("Juan");
			user.setLastname("Arevalo");
			user.setEmail("juanarevalo1020@gmail.com");
			
			mail.setTo(user.getEmail());
			mail.setFrom("juanarevalo1020@gmail.com");
			mail.setSubject("prueba tarea programada");
			mail.setText("esta es una prueba usando tarea progamada cron en lenguaje java");
			
		} catch (MailException e) {
			logger.error("Error no se pudo enviar email " + e.getMessage());
		}
		
		javamail.send(mail);

	}

}
