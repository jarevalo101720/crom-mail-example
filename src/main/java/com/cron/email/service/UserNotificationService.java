package com.cron.email.service;

import com.cron.email.dto.UserDto;

public interface UserNotificationService {

	public void userNotification(UserDto user);
}
