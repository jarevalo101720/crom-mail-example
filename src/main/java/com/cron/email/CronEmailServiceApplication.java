package com.cron.email;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CronEmailServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CronEmailServiceApplication.class, args);
	}

}
